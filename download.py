import boto3
import botocore
import concurrent.futures
import time
import os
import json



BUCKET_NAME = 'app.globoesporte.globo.com' 
KEY = 'al' # replace with your object key
ACCES_KEY = 'AKIAI4YFZ5M2TVFCUP4Q'
SECRET = 'XbcMazFvYalzWeKHnb0MPDg0u1sXMpUE9u0AS+Vh'

s3 = boto3.resource('s3', 
                    aws_access_key_id=ACCES_KEY,
                    aws_secret_access_key=SECRET)

def sleeper(nome):
    print(nome)
    folder = nome[0:nome.rfind("/")]
    folder =os.getcwd()+"/nao-processados/"+folder
    print(folder)
    print()
    if len(folder) > 0:
        if not os.path.exists(folder):
            os.makedirs(folder)

    s3.Bucket(BUCKET_NAME).download_file(nome, os.getcwd()+"/nao-processados/"+nome) 

def startTreads(nomes):     
    print("\nSTARTING SAVING FILES....")
    with concurrent.futures.ThreadPoolExecutor(max_workers=20) as executor:
        fs = [executor.submit(sleeper, nome) for nome in nomes]
        concurrent.futures.wait(fs)

def getNomes():
    init = time.time()
    nomes = []
    try:
        bucket = s3.Bucket(BUCKET_NAME)
        count = 0
        print(bucket)
        print(type(bucket))
        print()
        nomes = {"nomes":[]}

        if os.path.isfile(os.getcwd()+"/nomes.json"):
            with open('nomes.json', 'r') as f:
                data = json.load(f)
                nomes = data["nomes"]
                startTreads(nomes)

        else:
            for object in bucket.objects.all():
                if 'geradas' not in object.key and 'logs' not in object.key and 'tmp' not in object.key and 'TEMP' not in object.key:
                    count += 1
                    nomes["nomes"].append(object.key)

                    # print(os.getcwd()+object.key.replace("/","###"))
                    # s3.Bucket(BUCKET_NAME).download_file(object.key, os.getcwd()+"/"+object.key.replace("/","###")) 
                    if count % 1000 == 0:
                        print(str(count)+' files counted   '+str(time.time()-init)+' elapsed')

            # f = open(os.getcwd()+"/nomes.txt", "w")
            # f.write(str(nomes))

            with open('nomes.json', 'w') as outfile:
                json.dump(nomes, outfile)

    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            raise

getNomes()